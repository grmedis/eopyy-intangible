
package com.intracom.ws.eopyy.intagible.recovery;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.intracom.ws.eopyy package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetExamPrescription_QNAME = new QName("http://eopyy.ws.intracom.com/", "getExamPrescription");
    private final static QName _GetExamPrescriptionResponse_QNAME = new QName("http://eopyy.ws.intracom.com/", "getExamPrescriptionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.intracom.ws.eopyy
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetExamPrescription }
     * 
     */
    public com.intracom.ws.eopyy.intagible.recovery.GetExamPrescription createGetExamPrescription() {

        return new com.intracom.ws.eopyy.intagible.recovery.GetExamPrescription();
    }

    /**
     * Create an instance of {@link EPrescription }
     * 
     */
    public com.intracom.ws.eopyy.intagible.recovery.EPrescription createEPrescription() {

        return new com.intracom.ws.eopyy.intagible.recovery.EPrescription();
    }

    /**
     * Create an instance of {@link GetExamPrescriptionResponse }
     * 
     */
    public com.intracom.ws.eopyy.intagible.recovery.GetExamPrescriptionResponse createGetExamPrescriptionResponse() {
        return new com.intracom.ws.eopyy.intagible.recovery.GetExamPrescriptionResponse();
    }

    /**
     * Create an instance of {@link ExaminationFirst }
     * 
     */
    public com.intracom.ws.eopyy.intagible.recovery.ExaminationFirst createExaminationFirst() {
        return new com.intracom.ws.eopyy.intagible.recovery.ExaminationFirst();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetExamPrescription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eopyy.ws.intracom.com/", name = "getExamPrescription")
    public JAXBElement<com.intracom.ws.eopyy.intagible.recovery.GetExamPrescription> createGetExamPrescription(com.intracom.ws.eopyy.intagible.recovery.GetExamPrescription value) {

        return new JAXBElement<com.intracom.ws.eopyy.intagible.recovery.GetExamPrescription>(_GetExamPrescription_QNAME, com.intracom.ws.eopyy.intagible.recovery.GetExamPrescription.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetExamPrescriptionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://eopyy.ws.intracom.com/", name = "getExamPrescriptionResponse")
    public JAXBElement<com.intracom.ws.eopyy.intagible.recovery.GetExamPrescriptionResponse> createGetExamPrescriptionResponse(com.intracom.ws.eopyy.intagible.recovery.GetExamPrescriptionResponse value) {
        return new JAXBElement<com.intracom.ws.eopyy.intagible.recovery.GetExamPrescriptionResponse>(_GetExamPrescriptionResponse_QNAME, com.intracom.ws.eopyy.intagible.recovery.GetExamPrescriptionResponse.class, null, value);
    }

}
